import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import cookie from './plugins/cookie'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import router from "./router";
import store from "./store";
import {DateTime, Settings} from "luxon";

Vue.config.productionTip = false;

store.commit('setTime', DateTime.local());

Settings.defaultLocale = "ru";

new Vue({
  vuetify,
  cookie,
  render: h => h(App),
  store,
  router
}).$mount('#app');


