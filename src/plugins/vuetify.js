import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#5c6bc0',
        secondary: '#7e57c2',
        accent: '#9c27b0',
        error: '#ef5350',
        warning: '#ff7043',
        info: '#78909c',
        success: '#66bb6a',
      },
      dark: {
        primary: '#3f51b5',
        secondary: '#673ab7',
        accent: '#9c27b0',
        error: '#f44336',
        warning: '#ff5722',
        info: '#607d8b',
        success: '#4caf50',
      }
    }
  }
});
