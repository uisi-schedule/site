import axios from "axios";

let client = axios.create({
  baseURL: process.env.VUE_APP_SCHEDULE_API,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type, access-control-allow-headers, access-control-allow-origin, authorization',
  }
});

export { client }
