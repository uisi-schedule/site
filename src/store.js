import Vue from 'vue'
import Vuex from 'vuex'
import {DateTime} from "luxon";
import {client} from "./client"
import router from "./router"

Vue.use(Vuex);

import {set} from 'tiny-cookie'

export default new Vuex.Store({
  state: {
    dataState: String,
    groups: [],
    teachers: [],
    date: DateTime,
    days: [],
    scheduleLoading: Boolean,
    params: Object,
    name: String
  },
  mutations: {
    dataState(state, payload) {
      state.dataState = payload
    },
    route(state, payload) {
      state.name = payload.name;
      state.params = payload.params;
    },
    setTime(state) {
      state.date = DateTime.local()
    },
    previousWeek(state) {
      state.date = state.date.minus({weeks: 1});
    },
    nextWeek(state) {
      state.date = state.date.plus({weeks: 1});
    },
    setDays(state, days) {
      state.days = days
    },
    groups(state, payload) {
      state.groups = payload
    },
    teachers(state, payload) {
      state.teachers = payload
    },
    scheduleLoading(state, value) {
      state.scheduleLoading = value
    }
  },
  actions: {
    nextWeek({commit, getters}) {
      commit('nextWeek');
      return router.push(getters.route)
    },
    previousWeek({commit, getters}) {
      commit('previousWeek');
      return router.push(getters.route)
    },
    loadDates({commit}, path) {
      commit('scheduleLoading', true);
      return client.get(path)
        .then(response => response.data)
        .then(json => {
            commit('setDays', json.days);
            commit('scheduleLoading', false);
          }
        );
    },
    loadData({commit}) {
      commit('dataState', 'loading');
      return Promise.all([
        client.get('/groups'),
        client.get('/teachers')])
        .then(res => {
            commit('groups', res[0].data);
            commit('teachers', res[1].data);
            commit('dataState', 'loaded');
          }
        )
    },
    route({commit}, payload) {
      set('schedule.params', JSON.stringify(payload), {expires: '1M'});
      commit('route', payload)
    },
    launch({commit}) {
      commit('dataState', 'unloaded');
    }
  },
  getters: {
    dataState: state => state.dataState,
    weekBegin: state => state.date.startOf('week').toISODate(),
    weekEnd: state => state.date.endOf('week').minus({day: 1}).toISODate(),
    route: (state, getters) =>
      ({
        name: state.name,
        params: state.params,
        query:
          {
            from: getters.weekBegin,
            to: getters.weekEnd
          }
      }),
    hasSchedule: state => state.days.length > 0,
    scheduleLoading: state => state.scheduleLoading,
    groups: state => state.groups,
    teachers: state => state.teachers,
    text: state => Object.values(state.params)[0]
  }
})
