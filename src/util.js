import {DateTime} from "luxon";

let types = new Map();
types.set('Practice', 'Практика');
types.set('Exam', 'Экзамен');
types.set('Laboratory', 'Лабораторная');
types.set('Lecture', 'Лекция');
types.set('Test', 'Зачет');

let typesColors = new Map();
typesColors.set('Practice', '#2196F3');
typesColors.set('Exam', '#B71C1C');
typesColors.set('Laboratory', '#009688');
typesColors.set('Lecture', '#4CAF50');
typesColors.set('Test', '#FF5722');

let graduateLevels = new Map();
graduateLevels.set('College', 'Колледж');
graduateLevels.set('Bachelor', 'Бакалавриат');
graduateLevels.set('Master', 'Магистратура');

function graduateLevel(level) {
  return graduateLevels.get(level)
}

function lessonType(type) {
  return types.get(type)
}

function typeColor(type) {
  return typesColors.get(type)
}

function formatHeader(date) {
  return DateTime.fromISO(date).toFormat('cccc, d MMM');
}

function formatTime(date) {
  return DateTime.fromISO(date).toFormat('HH:mm');
}

export {
  lessonType,
  typeColor,
  formatHeader,
  graduateLevel,
  formatTime
}
