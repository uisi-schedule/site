import Vue from 'vue'
import Router from 'vue-router'

import FirstChoose from './components/menu/FirstChoose'
import ScheduleView from "./components/schedule/ScheduleHolder";
import Start from "./components/Start";

Vue.use(Router);

let router = new Router({
  // eslint-disable-next-line no-unused-vars
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0}
  },
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'start',
      component: Start
    },
    {
      path: '/home',
      name: 'firstChoose',
      component: FirstChoose
    },
    {
      path: '/teachers/:teacher/schedule/',
      name: 'byTeacher',
      component: ScheduleView
    },
    {
      path: '/groups/:group/schedule/',
      name: 'byGroup',
      component: ScheduleView
    },
    {
      path: '*',
      name: 'notFouynd',
      component: FirstChoose
    },
  ]
});

export default router
